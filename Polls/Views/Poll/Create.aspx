﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Polls.Models.ViewModels.PollCreateView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    New poll</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <h2>New poll</h2>
    
    <% using (Html.BeginForm("Create", "Poll"))
       {%>
    <%= Html.ValidationSummary(true, "Poll information incorrect. Please correct the errors and try again.")%>

    <fieldset>    
        <legend>Poll</legend>
        
        <table>
            <tr>
                <td>
                    <div class="editor-label">
                        <%= Html.LabelFor( poll => poll.Title ) %>
                    </div>
                </td>
                <td>
                    <div class="editor-field">
                        <%= Html.TextBoxFor( poll => poll.Title ) %>
                        <%= Html.ValidationMessageFor( poll => poll.Title) %>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="editor-label">
                        <%= Html.LabelFor( poll => poll.Description ) %>
                    </div>
                </td>
                <td>
                    <div class="editor-field">
                        <%= Html.TextAreaFor( poll => poll.Description ) %>
                        <%= Html.ValidationMessageFor( poll => poll.Description) %>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="editor-label">
                        <%= Html.LabelFor(poll => poll.Answers ) %>
                    </div>
                </td>
                <td>
                    <div class="editor-field">
                        <ol>
                            <% for (int i = 0; i < Model.Answers.Count(); i++)
                               { %>
                            <li>
                                <%= Html.TextBoxFor(poll => poll.Answers[i]) %>
                            </li>
                            <% } %>
                        </ol>
                        <%= Html.ValidationMessageFor( poll => poll.Answers) %>
                    </div>
                </td>
            </tr>
        </table>
        
        <%= Html.AntiForgeryToken() %>
        
        <p><input type="submit" value="Create" /></p>
    </fieldset>    
    <% } %>    
    <p><%= Html.ActionLink("Back", "Index") %></p>
</asp:Content>
