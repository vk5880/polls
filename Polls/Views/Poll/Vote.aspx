﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Polls.Models.Domain.Poll>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Vote
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    
    
    <h2>Vote</h2>
    
    <p><%= Html.Encode(ViewData["Message"]) %></p>
    
    <% using (Html.BeginForm("Vote", "Poll")) {%>    
    <fieldset>
        <legend><%= Html.Encode(Model.Title) %></legend>                       
        
        <p><%= Html.Encode(Model.Description) %></p>

        <% using (Html.BeginForm("Vote", "Poll")) { %>
            <table>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Answer
                    </th>
                    <th>
                        Votes
                    </th>
                </tr>
                <% bool selected = true;
                   if (Model.Answers != null)
                   {
                       foreach (Polls.Models.Domain.PollAnswer pollAnswer in Model.Answers)
                       { %>
                <tr>
                    <td>
                        <%= Html.RadioButton("pollAnswerID", pollAnswer.PollAnswerID, selected)%>
                    </td>
                    <td>
                        <%= Html.Encode(pollAnswer.Answer)%>
                    </td>
                    <td>
                        <%= pollAnswer.Votes %>
                    </td>
                </tr>
                <% selected = false; %>
                <% }
               } %>
            </table>
            
            <%= Html.AntiForgeryToken() %>
            <input type="submit" value="Vote" />
        <% } %><br />
        
        <table>
            <tr>
                <td>
                    <p>Status: <%= Model.Open ? "Opened" : "Closed" %></p>
                </td>
                <td>
                    <% using (Html.BeginForm("Close", "Poll"))
                       { %>
                        <%= Html.Hidden("pollId", Model.PollID)%>
                        <%= Html.AntiForgeryToken()%>
                    <input type="submit" value="Close" />
                    <% } %>
                </td>
            </tr>       
        </table>       
    </fieldset>
    <% } %>
    
    <p> Created <%= Model.TimeStamp.ToShortDateString() %> by <b><%= Html.Encode(Membership.GetUser(Model.Owner, false).UserName) %></b></p>
    
    
    
    <div><%= Html.ActionLink("Back", "Index") %></div>
</asp:Content>
