﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Polls.Models.Domain.Poll>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Results</h2>
    
    <p><%= Html.Encode(ViewData["Message"]) %></p>       
        
    <fieldset>
        <legend><%= Html.Encode(Model.Title) %></legend>                      
        
        <p><%= Html.Encode(Model.Description) %></p>
    
        <table>
            <tr>
                <th>#</th>
                <th>Answer</th>
                <th>Votes</th>
            </tr>
            
            <% if (Model.Answers != null) {
                   int index = 1;
                foreach (Polls.Models.Domain.PollAnswer pollAnswer in Model.Answers) { %>
            <tr>
                <td><%= index++%></td>
                <td><%= Html.Encode(pollAnswer.Answer)%></td>
                <td><%= pollAnswer.Votes %></td>
            </tr>            
            <% }
           } %>
        </table><br />
        
        <table>
            <tr>
                <td>
                    <p>Status: <%= Model.Open ? "Opened" : "Closed" %></p>
                </td>
                <td>
                    <% using (Html.BeginForm("Delete", "Poll"))
                       { %>
                        <%= Html.Hidden("pollId", Model.PollID)%>
                        <%= Html.AntiForgeryToken()%>
                    <input type="submit" value="Delete" />
                    <% } %>
                </td>
            </tr>       
        </table>
    </fieldset>
    
    <p> Created <%= Model.TimeStamp.ToShortDateString() %> by <b><%= Html.Encode(Membership.GetUser(Model.Owner, false).UserName) %></b></p>
    
    <div><%= Html.ActionLink("Back", "Index") %></div>
</asp:Content>
