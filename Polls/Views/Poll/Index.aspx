﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Polls.Models.Domain.Poll>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Home Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <p><% if(TempData["message"] != null) { %>
        <%= Html.Encode(TempData["Message"]) %>
    <% } %></p>

    <h2>List of polls</h2>
          
    <p><%= Html.ActionLink("New poll", "Create") %></p>
    
    <table>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Status</th>
            <th>Author</th>
        </tr>
        <% int index = 1;
        foreach( Polls.Models.Domain.Poll poll in Model) { %>
        <tr>
            <td><%= index++ %></td>
            <td><%= Html.ActionLink(Html.Encode(poll.Title), "Select", new { pollID = poll.PollID }) %></td>
            <td><%= poll.Open ? "Open": "Closed" %></td>
            <td><%= Html.Encode(Membership.Provider.GetUser(poll.Owner, false).UserName) %></td>
        </tr>
        <% } %>
    </table>
</asp:Content>
