﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Polls.Models;
using System.Web.Security;
using Polls.Repository;
using Polls.Models.Domain;
using Polls.Models.ViewModels;

namespace Polls.Controllers
{
    public class PollController : Controller
    {
        private readonly IPollRepository _pollRepository;
        private readonly IPollAnswerRepository _pollAnswerRepository;

        public PollController()
        {
            _pollRepository = PollFactoryProvider.GetPollFactory().GetPollRepository();
            _pollAnswerRepository = PollFactoryProvider.GetPollFactory().GetPollAnswerRepository();
        }

        [NonAction]
        private Guid GetCurrentUser()
        {
            if(User.Identity.IsAuthenticated)
                return (Guid)Membership.GetUser(User.Identity.Name).ProviderUserKey;
            return Guid.Empty;
        }

        //List of polls
        // GET: /Poll/
        [HttpGet]
        public ActionResult Index()
        {
            var polls = _pollRepository.GetPolls();

            return View(polls);
        }

        // GET: /Poll/Create
        [HttpGet]
        [Authorize]
        public ActionResult Create()
        {
            var poll = new PollCreateView(Poll.MaxAnswers);

            return View(poll);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PollCreateView pollCreateView)
        {            
            if (ModelState.IsValid)
            {
                Poll poll = new Poll(pollCreateView.Title, pollCreateView.Description, GetCurrentUser(), pollCreateView.Answers);
                try
                {
                    _pollRepository.Create(poll);

                    TempData["message"] = string.Format("New poll '{0}' have been created", poll.Title);

                    return RedirectToAction("Index");
                }
                catch(Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            return View(pollCreateView);
        }

        // Poll page
        // GET: /Poll/Details/5
        [HttpGet]
        public ActionResult Select(Guid? pollID)
        {
            if (!pollID.HasValue) return RedirectToAction("Index", "Poll");
            var poll = _pollRepository.GetPollByID(pollID.Value);
            if (poll == null) throw new HttpException(404, "Poll not found");

            if (poll.Open)
                return View("Vote", poll);
            else
                return View("Results", poll);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Vote(Guid? pollAnswerID)
        {
            if (!pollAnswerID.HasValue || pollAnswerID.Value == null) return RedirectToAction("Index");
            var pollAnswer = _pollAnswerRepository.GetPollAnswerByID(pollAnswerID.Value);
            if (pollAnswer == null) throw new HttpException(404, "PollAnswer not found");

            pollAnswer.Votes++;
            _pollAnswerRepository.Update(pollAnswer);

            TempData["message"] = "Thank you for your answer";

            return RedirectToAction("Index");
        }
        
        [HttpPost]
        //[Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Close(Guid? pollID)
        {
            if (!pollID.HasValue || pollID.Value == null) return RedirectToAction("Index");

            try
            {
                if (!User.Identity.IsAuthenticated) return RedirectToAction("LogOn", "Account", new { returnUrl = Request.UrlReferrer.PathAndQuery });
                
                var poll = _pollRepository.GetPollByID(pollID.Value);
                if (poll == null) throw new HttpException(404, "Poll not found");

                //owner validation
                if (poll.Owner != GetCurrentUser()) return RedirectToAction("LogOn", "Account", new { returnUrl = Request.UrlReferrer.PathAndQuery });

                if (poll.Open)
                {
                    poll.Open = false;
                    _pollRepository.Update(poll);

                    TempData["message"] = string.Format("Poll '{0}' sucessfully closed", poll.Title);
                }
                else
                    TempData["message"] = string.Format("Poll '{0}' already closed", poll.Title); 

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Select", new { pollID = pollID });
            }
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid? pollID)
        {
            if (!pollID.HasValue || pollID.Value == null) return RedirectToAction("Index");
            try
            {
                if (!User.Identity.IsAuthenticated) RedirectToAction("LogOn", "Account", new { returnUrl = Request.UrlReferrer.PathAndQuery });
                
                var poll = _pollRepository.GetPollByID(pollID.Value);
                if (poll == null) throw new HttpException(404, "Poll not found");

                //owner validation
                if (poll.Owner != GetCurrentUser()) return RedirectToAction("LogOn", "Account", new { returnUrl = Request.UrlReferrer.PathAndQuery });

                _pollRepository.Delete(poll);

                TempData["Message"] = string.Format("Poll '{0}' sucessfully deleted", poll.Title);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Select", new {pollID = pollID});
            }            
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
