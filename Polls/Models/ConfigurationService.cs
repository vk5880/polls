﻿using System.Configuration;
namespace Polls.Models
{
    public class ConfigurationService
    {
        public string GetPollsDBConnectionString()
        {
            return GetConfigValue("ApplicationServices");
        }

        private string GetConfigValue(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ToString();
        }
    }
}
