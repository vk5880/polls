﻿using System;
using System.Collections.Generic;
using System.Linq;
using Polls.Repository;

namespace Polls.Models.Domain
{
    public class Poll
    {
        public const int MinAnswers = 2;
        public const int MaxAnswers = 8;

        /// <summary>
        /// Existed entity constructor
        /// </summary>
        public Poll()
        {
        }

        /// <summary>
        /// New entity constructor
        /// </summary>
        /// <param name="owner"></param>
        public Poll(string title, string description, Guid owner, List<string> answers)
        {
            PollID = Guid.NewGuid();
            Title = title;
            Description = description;
            Open = true;
            Owner = owner;
            TimeStamp = DateTime.Now;

            _answers = new List<PollAnswer>(answers.Count());
            foreach (var answer in answers)
            {
                if(!string.IsNullOrEmpty(answer)) _answers.Add(new PollAnswer(answer, PollID));
            }
        }

        public Guid PollID { get; set; }
        public string Title  { get; set; }
        public string Description  { get; set; }
        public bool Open { get; set; }
        public Guid Owner { get; set; }
        public DateTime TimeStamp { get; set; }

        private List<PollAnswer> _answers { get; set; }
        public List<PollAnswer> Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }        
    }
}
