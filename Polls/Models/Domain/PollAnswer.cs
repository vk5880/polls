﻿using System;
using System.Web.Mvc;
namespace Polls.Models.Domain
{
    [Bind(Include = "Answer")]
    public class PollAnswer
    {
        /// <summary>
        /// Existed entity constructor
        /// </summary>
        public PollAnswer()
        {

        }

        /// <summary>
        /// New entity constructor
        /// </summary>
        /// <param name="owner"></param>
        public PollAnswer(string answer, Guid pollID)
        {
            PollAnswerID = Guid.NewGuid();
            this.Answer = answer;
            PollID = pollID;
            TimeStamp = DateTime.Now;
        }        

        public Guid PollAnswerID { get; set; }
        public Guid PollID { get; set; }
        public string Answer { get; set; }
        public int Votes { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
