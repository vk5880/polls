﻿
using Polls.Models.Repository;
namespace Polls.Repository
{
    static class PollFactoryProvider
    {
        private readonly static IPollFactory _pollFactory = new L2SPollFactory();
        public static IPollFactory GetPollFactory()
        {
            return _pollFactory;
        }
    }
}