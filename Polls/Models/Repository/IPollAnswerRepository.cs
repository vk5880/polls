using System;
using System.Collections.Generic;
using Polls.Models.Domain;

namespace Polls.Models
{
    public interface IPollAnswerRepository
    {
        PollAnswer GetPollAnswerByID(Guid pollAnswerID);
        List<PollAnswer> GetPollAnswers(Poll poll);

        void Create(PollAnswer pollAnswer);        
        void Update(PollAnswer pollAnswer);
        void Delete(PollAnswer pollAnswer);
        void DeletePollAnswers(Poll poll);
    }
}
