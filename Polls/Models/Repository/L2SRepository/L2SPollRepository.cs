using System;
using System.Collections.Generic;
using Polls.Models.Domain;
using Polls.Repository;
//using Polls.Repository.L2SRepository;

namespace Polls.Models.Repository.L2SRepository
{
    class L2SPollRepository :
        L2SRepository<Models.Repository.L2SRepository.Poll, Models.Domain.Poll>, 
        IPollRepository
    {
        private readonly Connection _connection;
        private readonly IPollAnswerRepository _pollAnswerRepository;
        public L2SPollRepository(Connection connection, IPollAnswerRepository pollAnswerRepository) : base(connection)
        {
            if (connection == null)
                throw new ArgumentNullException("db");

            _connection = connection;
            _pollAnswerRepository = pollAnswerRepository;
        }
        public Domain.Poll GetPollByID(Guid pollID)
        {
            if (pollID == Guid.Empty)
                throw new ArgumentException("pollID");

            _connection.Open();
            Domain.Poll poll = base.Get(p => p.id == pollID);
            poll.Answers = _pollAnswerRepository.GetPollAnswers(poll);
            _connection.Close();

            return poll;
        }
        public List<Domain.Poll> GetPollsByTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
                throw new ArgumentNullException("title");

            _connection.Open();
            List<Domain.Poll> polls = base.GetAll(p => p.title.Contains(title));
            foreach (var poll in polls) poll.Answers = _pollAnswerRepository.GetPollAnswers(poll);
            _connection.Close();

            return polls;
        }
        public List<Domain.Poll> GetOpenedPolls()
        {
            _connection.Open();
            List<Domain.Poll> polls = base.GetAll(p => p.open);
            foreach (var poll in polls) poll.Answers = _pollAnswerRepository.GetPollAnswers(poll);
            _connection.Close();

            return polls;
        }
        public List<Domain.Poll> GetPolls()
        {
            _connection.Open();
            List<Domain.Poll> polls = base.GetAll();
            polls.Sort(new Comparison<Domain.Poll>((Domain.Poll poll1, Domain.Poll poll2) => poll1.TimeStamp.CompareTo(poll2.TimeStamp)) );
            foreach (var poll in polls) poll.Answers = _pollAnswerRepository.GetPollAnswers(poll);
            _connection.Close();

            return polls;
        }
        public List<Domain.Poll> GetPollsByOwner(Guid owner)
        {
            if (owner == Guid.Empty)
                throw new ArgumentException("owner");

            _connection.Open();
            List<Domain.Poll> polls = base.GetAll(p => p.owner == owner);
            foreach (var poll in polls) poll.Answers = _pollAnswerRepository.GetPollAnswers(poll);
            _connection.Close();

            return polls;
        }
        public void Create(Models.Domain.Poll poll)
        {
            if (poll == null)
                throw new ArgumentNullException("Poll");
            if (string.IsNullOrEmpty(poll.Title))
                throw new ArgumentNullException("Title");
            if (poll.Owner == Guid.Empty)
                throw new ArgumentNullException("Owner");
            
            _connection.Open();
            base.Add(poll);            
            _connection.Close(); 
            foreach (var pollAnswer in poll.Answers) _pollAnswerRepository.Create(pollAnswer);
            
        }
        public void Delete(Models.Domain.Poll poll)
        {
            if (poll == null)
                throw new ArgumentNullException("Poll");

            _pollAnswerRepository.DeletePollAnswers(poll);            
            _connection.Open();            
            base.Delete(poll);
            _connection.Close();
        }
        public void Update(Models.Domain.Poll poll)
        {
            if (poll == null)
                throw new ArgumentNullException("Poll");
            if (string.IsNullOrEmpty(poll.Title))
                throw new ArgumentNullException("poll.Title");
            if (poll.Owner == Guid.Empty)
                throw new ArgumentNullException("Owner");

            _connection.Open();
            base.Update(poll, p => p.id == poll.PollID);
            
            _connection.Close();
        }
    }
}
