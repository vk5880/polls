﻿
using AutoMapper;
using Polls.Models;
using Polls.Models.Repository;

namespace Polls.Models.Repository.L2SRepository
{
    class L2SRepositoryConfiguration : IRepositoryConfiguration
    {
        public void Configure()
        {
            ConfigureAutoMapper();
        }

        public void ConfigureAutoMapper()
        {
            Mapper.CreateMap<Models.Domain.Poll, Models.Repository.L2SRepository.Poll>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.PollID))
                .ForMember(dest => dest.timestamp, opt => opt.MapFrom(src => src.TimeStamp));

            Mapper.CreateMap<Models.Repository.L2SRepository.Poll, Models.Domain.Poll>()
                .ForMember(dest => dest.PollID, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.TimeStamp, opt => opt.MapFrom(src => src.timestamp));

            Mapper.CreateMap<Models.Domain.PollAnswer, Models.Repository.L2SRepository.PollAnswer>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.PollAnswerID))
                .ForMember(dest => dest.poll_id, opt => opt.MapFrom(src => src.PollID));
                //.ForMember(dest => dest., opt => opt.MapFrom(src => src.Owner));

            Mapper.CreateMap<Models.Repository.L2SRepository.PollAnswer, Models.Domain.PollAnswer>()
                .ForMember(dest => dest.PollAnswerID, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.PollID, opt => opt.MapFrom(src => src.poll_id));


        }
    }
}