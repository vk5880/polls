﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Polls.Models.Repository.L2SRepository
{
    public class Connection : IDisposable
    {
        private PollsDataContext _dc;
        private string _connectionString;

        public Connection(ConfigurationService configurationService)
        {
            _connectionString = configurationService.GetPollsDBConnectionString();
        }

        public PollsDataContext Context
        {
            get { return _dc; }
            set { _dc = value; }
        }

        public void Open()
        {
            _dc = new PollsDataContext(_connectionString);
        }

        public void Close()
        {
            if (_dc != null) 
            {
                _dc.SubmitChanges();
                _dc.Dispose();
                _dc = null;
            }
        }

        public void SubmitChanges()
        {
            _dc.SubmitChanges();
        }

        public void RollbackChanges()
        {
            _dc.Transaction.Rollback();
        }

        public void Dispose()
        {
            Close();
        }
    }
}
