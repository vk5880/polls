using System;
using System.Collections.Generic;
using Polls.Models.Repository.L2SRepository;
//using Polls.Repository.L2SRepository;

namespace Polls.Models
{
    public class L2SPollAnswerRepository : 
        L2SRepository<Polls.Models.Repository.L2SRepository.PollAnswer, Domain.PollAnswer>,
        IPollAnswerRepository
    {
        private readonly Connection _connection;
        public L2SPollAnswerRepository(Connection connection ) : base(connection)
        {
            if (connection == null)
                throw new ArgumentNullException("connection");
            _connection = connection;
        }
        public Domain.PollAnswer GetPollAnswerByID(Guid pollAnswerID)
        {
            if (pollAnswerID == Guid.Empty)
                throw new ArgumentException("pollAnswerID");

            _connection.Open();
            var result = base.Get(pa => pa.id == pollAnswerID);
            _connection.Close();

            return result;
        }
        public List<Domain.PollAnswer> GetPollAnswers(Domain.Poll poll)
        {
            if (poll == null)
                throw new ArgumentNullException("poll");

            _connection.Open();
            var result = base.GetAll(pa => pa.poll_id == poll.PollID);
            _connection.Close();

            return result;
        }
        public void Create(Domain.PollAnswer pollAnswer)
        {
            if (pollAnswer == null)
                throw new ArgumentNullException("pollAnswer");

            if (string.IsNullOrEmpty(pollAnswer.Answer))
                throw new ArgumentNullException("pollAnswer.Answer");

            _connection.Open();
            base.Add(pollAnswer);            
            _connection.Close();
        }

        public void Delete(Domain.PollAnswer pollAnswer)
        {
            if (pollAnswer == null)
                throw new ArgumentNullException("PollAnswer");

            _connection.Open();
            base.Delete( pollAnswer );
            _connection.Close();
        }

        public void DeletePollAnswers(Domain.Poll poll) 
        {
            if (poll == null) throw new ArgumentNullException("poll");
            _connection.Open();
            base.Delete(pa => pa.poll_id == poll.PollID);
            _connection.Close();
        }

        public void Update(Domain.PollAnswer pollAnswer)
        {
            if (pollAnswer == null)
                throw new ArgumentNullException("PollAnswer");

            if (string.IsNullOrEmpty(pollAnswer.Answer))
                throw new ArgumentNullException("pollAnswer.Answer");

            _connection.Open();
            base.Update(pollAnswer, pa => pa.id == pollAnswer.PollAnswerID);
            _connection.Close();
        }
    }
}
