﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Reflection;
using System.Linq.Expressions;

namespace Polls.Models.Repository.L2SRepository
{
    public abstract class L2SRepository<TEntity, TPoco>
        where TEntity: class, new()
        where TPoco: class, new()
    {
        private readonly Connection _connection;
        public L2SRepository(Connection connection)
        {
            _connection = connection;
        }

        private TEntity GetEntity(Expression<Func<TEntity, bool>> query)
        {
            return _connection.Context.GetTable<TEntity>().Where(query).SingleOrDefault();
        }

        public TPoco Get(Expression<Func<TEntity, bool>> query)
        {
            TEntity entity = GetEntity(query);
            if(entity == null ) return null;
            return Mapper.Map<TEntity, TPoco>(entity) ;
        }

        public List<TPoco> GetAll(Expression<Func<TEntity, bool>> query)
        {
            IEnumerable<TEntity> pocos = null;
            if (query == null)
                pocos = _connection.Context.GetTable<TEntity>();
            else
                pocos = _connection.Context.GetTable<TEntity>().Where(query);

            if (pocos == null || pocos.Count() == 0) return null; //new List<TPoco>()
            return Mapper.Map<IEnumerable<TEntity>, List<TPoco>>(pocos);//, new List<TPoco>()
        }

        public void Add(TPoco poco)
        {
            TEntity entity = Mapper.Map(poco, new TEntity());

            _connection.Context.GetTable<TEntity>().InsertOnSubmit(entity);
        }

        public void Update(TPoco poco, bool commitNow) //= false
        {
            TEntity entity = Mapper.Map(poco, new TEntity());

            _connection.Context.GetTable<TEntity>().Attach(entity, true);
        }

        public void Update(TPoco poco, Expression<Func<TEntity, bool>> query)
        {
            TEntity entity = Mapper.Map(poco, new TEntity());

            TEntity entityFromDB = GetEntity(query); //_connection.Context.GetTable<TEntity>()
                //.Where(query).SingleOrDefault();
            if(entityFromDB == null)
                throw new NullReferenceException("Query supplied to get entity from DB is invalid, NULL value returned");

            PropertyInfo[] properties = entityFromDB.GetType().GetProperties();
            foreach(PropertyInfo property in properties)
            {
                object propertyValue = null;
                if(property.GetSetMethod() != null)
                {
                    PropertyInfo entityProperty = entity.GetType().GetProperty(property.Name);
                    if(entityProperty.PropertyType.BaseType == Type.GetType("System.ValueType") || 
                        entityProperty.PropertyType == Type.GetType("System.String"))
                    {
                        propertyValue = entity.GetType().GetProperty(property.Name).GetValue(entity, null);
                    }
                }
                if(propertyValue != null)
                    property.SetValue(entityFromDB, propertyValue, null);
            }

        }

        public void Delete(TPoco poco)
        {
            TEntity entity = Mapper.Map<TPoco, TEntity>(poco);
            TEntity dbEntity = _connection.Context.GetTable<TEntity>().Single(e => e == entity);
            if (entity != null) _connection.Context.GetTable<TEntity>().DeleteOnSubmit(dbEntity);
        }

        public void Delete(Expression<Func<TEntity, bool>> query)
        {            
            //GetEntity(query); //connection.Context.GetTable<TEntity>()
            //Where(query).SingleOrDefault(); 

            IEnumerable<TEntity> entities = _connection.Context.GetTable<TEntity>().Where(query);
            if (entities != null && entities.Count() > 0)
                _connection.Context.GetTable<TEntity>().DeleteAllOnSubmit(entities);
        }

        protected List<TPoco> GetAll()
        {
            return GetAll(null);
        }
    }
}
