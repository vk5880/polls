using System;
using Polls.Models.Repository.L2SRepository;

namespace Polls.Models.Repository
{
    class L2SPollFactory : IPollFactory
    {
        private readonly Connection _connection;
        private readonly L2SPollRepository _pollRepository;
        private readonly L2SPollAnswerRepository _pollAnswerRepository;
        private readonly L2SRepositoryConfiguration _repositoryConfiguration;
        
        public L2SPollFactory()
        {
            _repositoryConfiguration = new L2SRepositoryConfiguration();
            _connection = new Connection(new ConfigurationService());
            _pollAnswerRepository = new L2SPollAnswerRepository(_connection);
            _pollRepository = new L2SPollRepository(_connection, _pollAnswerRepository);                        
        }

        #region IPollFactory Members

        public IPollRepository GetPollRepository()
        {
            return _pollRepository;
        }
        public IPollAnswerRepository GetPollAnswerRepository()
        {
            return _pollAnswerRepository;
        }       
        public IRepositoryConfiguration GetRepositoryConfiguration()
        {
            return _repositoryConfiguration;
        }

        #endregion
    }
}
