﻿
namespace Polls.Models.Repository
{
    interface IRepositoryConfiguration
    {
        void Configure();
    }
}
