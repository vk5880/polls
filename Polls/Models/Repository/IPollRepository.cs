using System;
using System.Collections.Generic;
using Polls.Models.Domain;

namespace Polls.Models
{
    interface IPollRepository
    {
        Poll GetPollByID(Guid pollID);
        List<Poll> GetPollsByTitle(string Title);
        List<Poll> GetPollsByOwner(Guid Owner);
        List<Poll> GetOpenedPolls();
        List<Poll> GetPolls();

        void Create(Poll poll);
        void Delete(Poll Poll);
        void Update(Poll Poll);
    }
}
