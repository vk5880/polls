﻿
namespace Polls.Models.Repository
{
    interface IPollFactory
    {
        IPollRepository GetPollRepository();
        IPollAnswerRepository GetPollAnswerRepository();
        IRepositoryConfiguration GetRepositoryConfiguration();
    }
}
