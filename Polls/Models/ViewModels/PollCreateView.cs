﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Polls.Models.Domain;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections;

namespace Polls.Models.ViewModels
{
    public class PollCreateView
    {
        [Required(ErrorMessage = "Input title please")]
        [DataType(DataType.Text)]
        public string Title { get; set; }
        [Required(ErrorMessage = "Input question please")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Question")]
        public string Description { get; set; }

        //[Min2Answers]
        [ValidateContainerElementCount( 2, true )]
        public List<string> Answers { get; set; }

        public PollCreateView() { }

        public PollCreateView(int answers)
        {
            Answers = new List<string>(new string[answers]);
        }
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class ValidateContainerElementCountAttribute : ValidationAttribute
    {
        private const string _doubleErrorMessage = "{0} must have at least {1} elements and at most {2} elements";
        private const string _minErrorMessage = "{0} must have at least {1} elements";
        private const string _maxErrorMessage = "{0} must have at most {2} elements";
        
        private readonly int _min, _max;

        public ValidateContainerElementCountAttribute(int limit, bool min)
            : base( min ? _minErrorMessage : _maxErrorMessage)
        {
            if (min)
            {
                _min = limit;
                _max = int.MaxValue;
            }
            else
            {
                _min = int.MinValue;
                _max = limit;
            }
        }

        public ValidateContainerElementCountAttribute(int min, int max)
            : base(_doubleErrorMessage)
        {
            _min = min;
            _max = max;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(ErrorMessageString, name, _min, _max);
        }

        public override bool IsValid(object value)
        {
            if (value is IEnumerable )
            {
                IEnumerable enumerable = value as IEnumerable;
                var enumerator = enumerable.GetEnumerator();
                int length = 0;
                while (enumerator.MoveNext())
                {
                    Type type = enumerator.Current.GetType();
                    if (type.IsValueType)
                    {
                        if (enumerator.Current != Activator.CreateInstance(type)) length++;
                    }
                    else if (type.Equals(typeof(string)))
                    {
                        if (!string.IsNullOrEmpty((string)enumerator.Current)) length++;
                    }
                    else
                    {
                        if (enumerator.Current != null) length++;
                    }
                }
                if ( length >= _min && length <= _max) return true;               
            }
            return false;            
        }
    }
}
